export default interface Authentication<Session, User> {
  session: Session;
  user: User;
}

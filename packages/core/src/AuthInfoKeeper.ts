import IAuthInfoKeeper, {AuthenticationOptions} from './IAuthInfoKeeper';
import Listener from './Listener';
import AsyncStorageFactory, {
  AsyncStorage,
  IStorageBackend,
} from '@react-native-community/async-storage';
import Authentication from './Authentication';

type StorageModel = {
  authentication: string;
};

// noinspection JSUnusedGlobalSymbols
export default class AuthInfoKeeper<Session, User> extends IAuthInfoKeeper<
  Session,
  User
> {
  private readonly storage: AsyncStorage<StorageModel, IStorageBackend<StorageModel>>;

  private readonly listeners: Listener<Session, User>[] = [];

  private initialized: boolean = false;

  private authentication: Authentication<Session, User> | undefined;

  private rememberAuthentication: boolean = false;

  constructor(storage: IStorageBackend) {
    super();
    if (!storage) throw new Error('storage must be provided');
    this.storage = AsyncStorageFactory.create<StorageModel>(storage);
  }

  // region Listener helpers
  private onInitialized(authentication: Authentication<Session, User> | undefined) {
    this.listeners.forEach(
      (listener) => listener.onInitialized && listener.onInitialized(authentication),
    );
  }

  private onAuthenticated(authentication: Authentication<Session, User>) {
    this.listeners.forEach(
      (listener) => listener.onAuthenticated && listener.onAuthenticated(authentication),
    );
  }

  private onUpdated(authentication: Authentication<Session, User>) {
    this.listeners.forEach(
      (listener) => listener.onUpdated && listener.onUpdated(authentication),
    );
  }

  private onReset() {
    this.listeners.forEach((listener) => listener.onReset && listener.onReset());
  }
  // endregion

  public async initialize() {
    if (this.initialized) throw new Error('Cannot initialize twice');
    const authentication = await this.readAuthentication();
    this.authentication = authentication;
    if (authentication) {
      this.rememberAuthentication = true;
    }
    this.initialized = true;
    this.onInitialized(authentication);
  }

  private async addAuthenticationInternal(
    authentication: Authentication<Session, User>,
    rememberAuthentication: boolean,
  ) {
    if (rememberAuthentication) {
      await this.saveAuthentication(authentication);
    }
    this.authentication = authentication;
    this.rememberAuthentication = rememberAuthentication;
    this.onAuthenticated(authentication);
  }

  public async authenticate(
    session: Session,
    user: User,
    options?: AuthenticationOptions,
  ) {
    this.checkInitialization();
    if (await this.isAuthenticated()) throw new Error('User already authenticated');
    const authentication: Authentication<Session, User> = {session, user};

    let rememberAuthentication: boolean = true;
    if (options && options.rememberAuthentication !== undefined) {
      rememberAuthentication = options.rememberAuthentication;
    }

    await this.addAuthenticationInternal(authentication, rememberAuthentication);
  }

  private async updateAuthenticationInternal(
    authentication: Authentication<Session, User>,
  ) {
    if (this.rememberAuthentication) {
      await this.saveAuthentication(authentication);
    }
    this.authentication = authentication;
    this.onUpdated(authentication);
  }

  public async updateSession(session: Session) {
    this.checkInitialization();
    const authentication = await this.getAuthentication();
    if (!authentication) throw new Error('User is not authenticated');

    await this.updateAuthenticationInternal({
      ...authentication,
      session,
    });
  }

  public async updateUser(user: User) {
    this.checkInitialization();
    const authentication = await this.getAuthentication();
    if (!authentication) throw new Error('User is not authenticated');

    await this.updateAuthenticationInternal({...authentication, user});
  }

  public isAuthenticated() {
    this.checkInitialization();
    return !!this.getAuthentication();
  }

  public getAuthentication() {
    this.checkInitialization();
    return this.authentication;
  }

  public getSession() {
    this.checkInitialization();
    const authentication = this.getAuthentication();
    if (!authentication) return undefined;
    return authentication.session;
  }

  public getUser() {
    this.checkInitialization();
    const authentication = this.getAuthentication();
    if (!authentication) return undefined;
    return authentication.user;
  }

  private async removeSessionInternal() {
    if (this.rememberAuthentication) {
      await this.saveAuthentication(undefined);
    }
    this.authentication = undefined;
    this.rememberAuthentication = false;
    this.onReset();
  }

  public async reset() {
    this.checkInitialization();
    const authentication = await this.getAuthentication();
    if (!authentication) throw new Error('User is not authenticated');

    await this.removeSessionInternal();
  }

  public addListener(listener: Listener<Session, User>) {
    this.listeners.push(listener);
  }

  public removeListener(listener: Listener<Session, User>) {
    const index = this.listeners.indexOf(listener);
    if (index > -1) {
      this.listeners.splice(index, 1);
    }
  }

  private async readAuthentication() {
    const json = await this.storage.get('authentication');
    if (!json) return undefined;
    return JSON.parse(json) as Authentication<Session, User>;
  }

  private async saveAuthentication(
    authentication: Authentication<Session, User> | undefined,
  ) {
    if (authentication) {
      const json = JSON.stringify(authentication);
      await this.storage.set('authentication', json);
    } else {
      await this.storage.remove('authentication');
      this.authentication = undefined;
    }
  }

  private checkInitialization() {
    if (!this.initialized)
      throw new Error('Not initialized yet. Please call initialize() method');
  }
}

import Authentication from './Authentication';
import Listener from './Listener';

export type AuthenticationOptions = {
  rememberAuthentication?: boolean;
};

export default abstract class IAuthInfoKeeper<Session, User> {
  abstract initialize(): Promise<void>;

  abstract authenticate(
    session: Session,
    user: User,
    options?: AuthenticationOptions,
  ): Promise<void>;

  abstract updateSession(session: Session): Promise<void>;

  abstract updateUser(user: User): Promise<void>;

  abstract isAuthenticated(): boolean;

  abstract getAuthentication(): Authentication<Session, User> | undefined;

  abstract getSession(): Session | undefined;

  abstract getUser(): User | undefined;

  abstract reset(): Promise<void>;

  abstract addListener(listener: Listener<Session, User>): void;

  abstract removeListener(listener: Listener<Session, User>): void;
}

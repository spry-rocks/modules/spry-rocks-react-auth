import Authentication from './Authentication';

type Listener<Session, User> = {
  onInitialized?: (authentication: Authentication<Session, User> | undefined) => void;
  onAuthenticated?: (authentication: Authentication<Session, User> | undefined) => void;
  onUpdated?: (authentication: Authentication<Session, User>) => void;
  onReset?: () => void;
};

export default Listener;

import {IAuthInfoKeeper as SRRA_IAuthInfoKeeper} from '@spryrocks/react-auth';
import {ContainerModule, interfaces} from 'inversify';

type ServiceIdentifier<T> =
  | string
  | symbol
  | interfaces.Newable<T>
  | interfaces.Abstract<T>;

export const createModule = <
  IAuthInfoKeeper extends SRRA_IAuthInfoKeeper<Session, User>,
  Session,
  User
>(options: {
  authInfoKeeper: {
    identifier: ServiceIdentifier<IAuthInfoKeeper>;
    constantValue?: IAuthInfoKeeper;
    createAuthInfoKeeper?: (context: interfaces.Context) => IAuthInfoKeeper;
  };
}) =>
  new ContainerModule((bind) => {
    if (options?.authInfoKeeper?.constantValue) {
      bind(options.authInfoKeeper.identifier).toConstantValue(
        options.authInfoKeeper.constantValue,
      );
    } else {
      bind(options.authInfoKeeper.identifier)
        .toDynamicValue((context) => {
          if (!options.authInfoKeeper.createAuthInfoKeeper)
            throw new Error(
              'Please provide options.authInfoKeeper.createAuthInfoKeeper function',
            );
          return options.authInfoKeeper.createAuthInfoKeeper(context);
        })
        .inSingletonScope();
    }
  });
